# Promise

[![Maven Central](https://img.shields.io/maven-central/v/net.thebugmc/promise)](https://search.maven.org/artifact/net.thebugmc/promise)
[![MIT License](https://img.shields.io/badge/license-BSD--2--Clause-blue)](https://gitlab.com/thebugmc/javadoc-tester/blob/master/LICENSE-BSD-2)

Java async programming toolset.

## Setup

> This library uses Java Virtual Threads. It may be used only under Java 21 or higher.

Here is an example of installing this library in Gradle:

```kts
dependencies {
    implementation("net.thebugmc:promise:2.2.0")
}
```

> Copy the proper library setup with the newest version from the [Maven Central](https://central.sonatype.com/artifact/net.thebugmc/promise).

## Usage

### Promises and Schedulers

The main piece of this library is the [`Promise`](src/main/java/net/thebugmc/async/Promise.java)
class - a task that will give a result at some point in the future. It takes in a [`Scheduler`](src/main/java/net/thebugmc/async/schedule/Scheduler.java)
and a function lambda.

Promises API allows you to chain the results. Each step of the Promise chain will await for the
previous step to complete, using or discarding the result.

```java
import net.thebugmc.async.Promise;

var scheduler = ...;

var helloWorld = Promise
    .get(scheduler, () -> "Hello") // creates the first Promise
    .map(scheduler, string -> string + " world!") // awaits for the first Promise to complete, modifies result
    .await(); // awaits for the chain to complete, blocks, gives resulting string
```

See documentation for each function for examples on how to use `Promise`s in your projects.

The `Scheduler` provided by default by this library is the [`VirtualThreadScheduler`](src/main/java/net/thebugmc/async/schedule/VirtualThreadScheduler.java).
It puts every new task on a new Virtual Thread. The default (that allows you to not mention a
scheduler when making a promise) **requires** you to create an instance of the
`VirtualThreadScheduler` using the `instance()` method. When you have an instance, all following
calls within the same thread will be using it. Tasks made with `VirtualThreadScheduler` also know
about the instance they were made with. As a bonus point, `VirtualThreadScheduler` is
`AutoCloseable`.

```java
import net.thebugmc.async.Promise;
import net.thebugmc.async.schedule.VirtualThreadScheduler;

try (var _ = VirtualThreadScheduler.instance()) {
    Promise
        .run(() -> System.out.println("Hello world!"))
        .thenRun(() -> System.out.println("This chain is using that scheduler instance."))
        .await();
}
```

### Mutex

[`Mutex`](src/main/java/net/thebugmc/async/mutex/Mutex.java) is a very useful way for you to create
critical sections. In reality, it is a simple wrapper for `ReentrantLock` with a more usable API.
There are two ways to use the mutex - with a value, and without.

#### Guaranteeing the atomicity within a critical section

If you do not define a specific type for a mutex (i.e. `Mutex<?>`) you cannot easily associate a
value with it. The main usage for it is to perform some actions while holding the lock, so that no
other thread may do the same actions at the same time - they will be blocked.

```java
import net.thebugmc.async.mutex.Mutex;

Mutex<?> criticalSection = Mutex.of();

try (var _ = criticalSection.access()) {
    // do something that nobody else should be doing at the same time
} // unlock happens here
```

#### Performing atomic value access

You can also associate a value with the mutex (i.e. `Mutex<String>`).

```java
import net.thebugmc.async.mutex.Mutex;

var helloWorld = Mutex.of("Hello");

try (var access = helloWorld.access()) {
    var value = access.get();
    System.out.println(value);
    access.set(value + " world!");
}
```

### Channels

[`Channel`](src/main/java/net/thebugmc/async/channel/Channel.java)s, or multi-producer,
multi-consumer, multi-value queues, allow you to perform communication between threads in a safe
way. General use is to pre-make some senders and receivers using the `tx()` and `rx()` methods, and
then pass the receiver to a thread, so that it will be in a reading loop, and pass sender to another
thread that will be sending some messages. Closing all senders will notify receivers to stop their
reading loops.

```java
import net.thebugmc.async.channel.Channel;

var channel = new Channel();
var sender = channel.tx();
var receiver = channel.rx();

// sending thread
try (var tx = sender) {
    tx.send("Hello world!");
} // `tx` is closed here

// receiving thread
try (var rx = receiver) {
    for (var msg : rx) // will end when `tx` is closed
        System.out.println(msg);
}
```

### Barrier

[`Barrier`](src/main/java/net/thebugmc/async/barrier/Barrier.java) allows you to synchronize threads
execution. You specify a number of threads to wait for when making a barrier. Then, when those
threads all call `await()` on the barrier, they all will be allowed to continue execution.

```java
var barrier = new Barrier(2);

// thread 1
System.out.print("Hello"); // will be guaranteed to perform first
barrier.await();

// thread 2
barrier.await();
System.out.println(" world!"); // will be guaranteed to perform last
```

For multiple threads doing the action in a particular order (by `currentThreadNumber`):

```java
import net.thebugmc.async.barrier.Barrier;

var threadCount = ...;
var barrier = new Barrier(threadCount);

// in each thread
var currentThreadNumber = ...; // 0 1 2 3 ...
for (var i = 0; i < threadCount; i++) {
    if (i == currentThreadNumber)
        System.out.println(currentThreadNumber); // will do 0 1 2 3 ...
    barrier.await();
}
```
