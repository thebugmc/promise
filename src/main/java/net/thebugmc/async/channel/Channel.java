package net.thebugmc.async.channel;

import net.thebugmc.async.Blocking;
import net.thebugmc.async.mutex.Mutex;

import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * A multi-producer, multi-consumer, multi-message queue channel.
 *
 * <p>
 * <b>Examples</b>
 *
 * <p>
 * This example creates a new channel and two virtual threads, one sends messages to the other.
 * {@snippet :
 * import net.thebugmc.async.channel.Channel;
 * import net.thebugmc.async.barrier.Barrier;
 * import java.util.ArrayList;
 * import java.util.List;
 *
 * var messages = new ArrayList<String>();
 *
 * var channel = new Channel<String>();
 * var startBarrier = new Barrier(2);
 * var txThread = Thread.startVirtualThread(() -> {
 *     try (var tx = channel.tx()) {
 *         startBarrier.await(); // barrier so `rx` is subscribed before `tx` sends any messages
 *         tx.post("hello");
 *         // `tx` auto-closes here, and `rx` is notified to end the for-each loop
 *     } catch (Exception ignored) {}
 * });
 * var rxThread = Thread.startVirtualThread(() -> {
 *     try (var rx = channel.rx()) {
 *         startBarrier.await();
 *         for (var message : rx) // this will end once `tx` closes
 *             messages.add(message); // only this thread accesses this list
 *     } catch (Exception ignored) {}
 * });
 *
 * txThread.join();
 * rxThread.join();
 *
 * assertEquals(List.of("hello"), messages);
 *}
 *
 * @param <T> The type of messages in the channel.
 */
public final class Channel<T> {
    private final Mutex<?> lock = new Mutex<>(); // cba to put all state into the mutex

    private final ArrayList<Sender> senders = new ArrayList<>();
    private final ArrayList<Receiver> receivers = new ArrayList<>();

    /**
     * Create a new sender that can send messages to this channel.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.channel.Channel;
     * import java.util.Optional;
     *
     * var channel = new Channel<String>();
     * try (var rx = channel.rx()) {
     *     try (var tx = channel.tx()) {
     *         tx.post("hello");
     *     }
     *     assertEquals(Optional.of("hello"), rx.get());
     * }
     *}
     *
     * @return A new sender.
     */
    @Blocking
    public Sender tx() {
        return new Sender();
    }

    /**
     * Create a new receiver subscribed to this channel.
     *
     * <p>
     * See {@link #tx()} for an example.
     *
     * @return A new receiver.
     */
    @Blocking
    public Receiver rx() {
        return new Receiver();
    }

    /**
     * A sender, or a producer, sends messages to the channel.
     *
     * <p>
     * See {@link #tx()} for an example.
     */
    public final class Sender implements AutoCloseable {
        private boolean open;

        @Blocking
        private Sender() {
            try (var ignored = lock.access()) {
                open = true;
                senders.add(this);
            }
        }

        private void sendAll(Mutex<?>.Access ignored, Optional<T> message) {
            for (var receiver : receivers) {
                if (!receiver.queue.getLast().complete(message))
                    throw new AssertionError("completing future twice");
                receiver.queue.addLast(new CompletableFuture<>());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored1) {
                }
            }
        }

        /**
         * Close this sender. This sender will no longer be able to post messages.
         *
         * <p>
         * When all senders are closed, all receivers are notified of that, but not closed. This
         * means they will end their current iteration, but they may go back to iterating again.
         *
         * @see Receiver
         */
        @Blocking
        public void close() {
            try (var access = lock.access()) {
                // mark this as closed
                open = false;
                senders.remove(this);

                // if no more senders, wake up waiting for new message ones
                if (senders.isEmpty())
                    sendAll(access, Optional.empty());
            }
        }

        /**
         * Post a message to the channel. All receivers will be woken up by it.
         *
         * <p>
         * The message will be queued for all receivers. If there are no receivers, the message is
         * lost.
         *
         * @param message The message to post.
         * @throws IllegalStateException If this sender is closed.
         */
        @Blocking
        public void post(T message) throws IllegalStateException {
            try (var access = lock.access()) {
                if (!open)
                    throw new IllegalStateException("This sender is invalid (closed)");
                sendAll(access, Optional.of(message));
            }
        }
    }

    /**
     * Receiver, or a consumer, consumes messages posted by the sender.
     *
     * <p>
     * Using this class within a single thread is recommended. See {@link #hasNext()}.
     *
     * <p>
     * See {@link #tx()} for an example.
     */
    public final class Receiver implements Iterable<T>, Iterator<T>, AutoCloseable {
        private boolean open;

        private final ArrayDeque<CompletableFuture<Optional<T>>> queue = new ArrayDeque<>();

        @Blocking
        private Receiver() {
            try (var ignored = lock.access()) {
                receivers.add(this);
                open = true;

                // always ensure queue has at least one slot
                queue.add(new CompletableFuture<>());
            }
        }

        /**
         * Close this receiver. This receiver will no longer be able to receive messages ({@link
         * #hasNext()} will return false).
         *
         * <p>
         * <b>Examples</b>
         * {@snippet :
         * import net.thebugmc.async.channel.Channel;
         * import java.util.Optional;
         *
         * var channel = new Channel<String>();
         * var tx = channel.tx();
         * var rx = channel.rx();
         *
         * tx.post("hello");
         * rx.close();
         * tx.post("world");
         * assertEquals(Optional.of("hello"), rx.get());
         * assertEquals(Optional.empty(), rx.get());
         *}
         */
        @Blocking
        public void close() {
            try (var ignored = lock.access()) {
                open = false;
                receivers.remove(this);
            }
        }

        /**
         * This iterable is an iterator in itself. This means that it can be used in a for-each
         * as-is, but also means that every new call of this method does not create a new iterator.
         *
         * <p>
         * See other methods for examples of using {@code for}-each loops.
         *
         * @return This receiver as an iterator.
         */
        public Iterator<T> iterator() {
            return this;
        }

        private Optional<T> current = Optional.empty();

        /**
         * Check if there is a new message from a queue or wait for a new message. If queue is empty
         * and this receiver is closed, this method will return false, as well as the case when all
         * senders were closed (see {@link Sender#close()}).
         *
         * <p>
         * Calling this method from multiple threads is not guaranteed to be thread-safe, so you are
         * to use instances of this class only from a single thread.
         *
         * <p>
         * <b>Examples</b>
         * {@snippet :
         * import net.thebugmc.async.channel.Channel;
         * import java.util.concurrent.atomic.AtomicBoolean;
         *
         * var channel = new Channel<String>();
         * var tx = channel.tx();
         * var rx = channel.rx();
         *
         * var hasRead1 = new AtomicBoolean();
         * var hasRead2 = new AtomicBoolean();
         *
         * var txThread = Thread.startVirtualThread(() -> {
         *     tx.post("hello");
         *     tx.close();
         * });
         * var rxThread = Thread.startVirtualThread(() -> {
         *     hasRead1.set(rx.hasNext());
         *     hasRead2.set(rx.hasNext());
         * });
         *
         * txThread.join();
         * rxThread.join();
         *
         * assertTrue(hasRead1.get());
         * assertFalse(hasRead2.get());
         *}
         *
         * @return If there are any messages to receive with this receiver.
         * @see Sender#close()
         * @see #next()
         */
        @Blocking
        public boolean hasNext() {
            CompletableFuture<Optional<T>> future;
            try (var ignored = lock.access()) {
                // there is nothing to read when non-open and queue is just an incomplete future
                if (!open && queue.size() <= 1)
                    return false;

                future = queue.getFirst();
            }
            current = future.join();
            try (var ignored = lock.access()) {
                queue.removeFirst();
            }
            return current.isPresent();
        }

        /**
         * Get the next message. Make sure that you call {@link #hasNext()} before every call to
         * this method, because you may receive a message using this method only once.
         *
         * <p>
         * See {@link #hasNext()} for explanations and examples.
         *
         * @return The next message.
         * @throws NoSuchElementException When you forgot to call {@link #hasNext()}, or when it
         *                                gives {@code false}, but you decided to proceed anyway.
         */
        public T next() throws NoSuchElementException {
            var value = current.orElseThrow(() -> new NoSuchElementException(
                "Called `next()` on a channel's receiver without grabbing value via "
                + "`hasNext()` first, or no `next()` value is available."
            ));
            current = Optional.empty();
            return value;
        }

        /**
         * Get a single next message.
         *
         * <p>
         * This method is equivalent to following code:
         * <pre> {@code
         * var result = hasNext()
         *     ? Optional.of(next())
         *     : Optional.empty();
         * }</pre>
         *
         * <p>
         * See {@link #tx()} for an example.
         *
         * @return The next message, or empty in case.
         */
        @Blocking
        public Optional<T> get() {
            return hasNext()
                ? Optional.of(next())
                : Optional.empty();
        }
    }
}
