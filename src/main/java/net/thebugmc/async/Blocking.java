package net.thebugmc.async;

import java.lang.annotation.*;

/**
 * Documentation-purpose annotation that indicates that the annotated method may perform blocking
 * operations. This means the executing thread may let other operations to perform concurrently
 * while it is waiting.
 *
 * <p>
 * Missing this annotation does <b>not</b> mean that the annotated method must not block (e.g.
 * method may call a callback that is blocking), nor having one present does not really mandate for
 * the method to be blocking (e.g. when a method may be blocking conditionally).
 *
 * <p>
 * If you are calling a {@link Blocking} method inside your method, you most likely want your method
 * to be annotated with {@link Blocking} as well.
 *
 * <p>
 * Calling a {@link Blocking} method while holding some sort of a lock, in most cases, is not a good
 * idea.
 */
@Retention(RetentionPolicy.CLASS)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
@Documented
public @interface Blocking {
}
