package net.thebugmc.async;

/**
 * Exception that may be thrown by {@link Promise}s.
 */
public class PromiseException extends RuntimeException {
    public PromiseException(Throwable cause) {
        super(cause);
    }
}
