package net.thebugmc.async.schedule;

import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Scheduler assigns a thread to perform an action in.
 */
public interface Scheduler extends Executor {
    default void execute(Runnable command) {
        schedule(command);
    }

    /**
     * Schedule to immediately run a task.
     *
     * @param task Action to perform.
     */
    Future<?> schedule(Runnable task);

    /**
     * Schedule to run a task after some period.
     *
     * @param delay Delay before the task is run.
     * @param unit  Delay unit.
     */
    Future<?> scheduleDelay(long delay, TimeUnit unit);

    /**
     * Schedule to repeatedly run a task.
     *
     * @param period Period between iterations.
     * @param unit   Period unit.
     * @param task   Action to perform.
     */
    Future<?> scheduleRepeating(long period, TimeUnit unit, Consumer<LoopState> task);
}
