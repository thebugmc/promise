package net.thebugmc.async.mutex;

import net.thebugmc.async.Blocking;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * {@link Mutex} is a thread-safe wrapper for a value with per-thread <i>mutual exclusion</i>. This
 * means only one thread may be accessing the stored value at a time, where others will have to
 * wait.
 *
 * <p>
 * For this API/Implementation, {@link Mutex} associates {@link ReentrantLock} with a value, and
 * lets users modify the value when an {@link Mutex.Access} is held. This allows for a simple
 * {@link AutoCloseable} try-with-resources usage pattern via {@link #access()}, while also allowing
 * for the thread to access the value in a reentrant fashion without blocking itself by accident. A
 * functional variant of this API is also available via {@link #access(Consumer)}.
 *
 * <p>
 * This class is not based around {@link Optional}, and having a {@link Mutex} with a {@code null}
 * value is a common occurrence. If you want to store {@link Optional} you have to do it explicitly.
 * See {@link Mutex.Access#uninitialized()}.
 *
 * <p>
 * <b>Caution</b>
 *
 * <p>
 * Be careful when pulling a value out of the {@link Mutex} - you might be mutating underlying value
 * without synchronization. The exceptions are immutable values (like {@link Number}s,
 * {@link String}s, {@link Record record}s of immutable values, etc.) and other stored
 * {@link Mutex}es and similar structures, since they have their own synchronization.
 *
 * <p>
 * <b>Examples</b>
 *
 * <p>
 * Following example shows how to use {@link Mutex} to synchronize an {@link ArrayList} of
 * {@link String}s.
 * {@snippet :
 * import java.util.List;
 * import java.util.ArrayList;
 * import net.thebugmc.async.mutex.Mutex;
 *
 * // creating a `Mutex`
 * var mutex = Mutex.of(new ArrayList<String>());
 *
 * // accessing the `Mutex` (from any thread)
 * try (var access = mutex.access()) { // blocking
 *     // lock happens here
 *     var list = access.get();
 *     list.add("hello");
 *     // unlock happens here
 * }
 *
 * // taking a value from a Mutex
 * var list = mutex.take(); // replaces stored value with `null`
 * assertEquals(List.of("hello"), list);
 *}
 *
 * <p>
 * This example shows how to create a simple critical section using a {@link Mutex}.
 * {@snippet :
 * import net.thebugmc.async.mutex.Mutex;
 *
 * // global
 * Mutex<?> criticalSection = Mutex.of();
 * int counter = 0;
 *
 * // in all threads
 * try (var ignored = criticalSection.access()) {
 *     // critical section here
 *     // atomic read + write is guaranteed
 *     var current = counter;
 *     counter = current + 1;
 * }
 *}
 *
 * @param <T> Stored value type. Nullable ({@link Mutex.Access#uninitialized() uninitialized}).
 */
public final class Mutex<T> {
    private final ReentrantLock lock = new ReentrantLock();

    private T value;

    /**
     * Creates an {@link Mutex.Access#uninitialized() uninitialized} {@link Mutex}. It is highly
     * recommended to give out this {@link Mutex} to other users only after filling it with a value.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.mutex.Mutex;
     *
     * var mutex = new Mutex<String>();
     * assertTrue(mutex.uninitialized());
     *}
     */
    public Mutex() {
    }

    /**
     * A static method alias of {@link #Mutex()} that creates an
     * {@link Mutex.Access#uninitialized() uninitialized} {@link Mutex}.
     *
     * @param <T> Stored value type.
     * @return An {@link Mutex.Access#uninitialized() uninitialized} {@link Mutex}.
     */
    public static <T> Mutex<T> of() {
        return new Mutex<>();
    }

    /**
     * Creates a {@link Mutex} initialized to some value.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.mutex.Mutex;
     *
     * var mutex = new Mutex<>("hello");
     * assertFalse(mutex.uninitialized());
     *}
     *
     * @param value Initial value.
     * @throws NullPointerException If {@code value} is {@code null}. Use {@link #Mutex()} instead
     *                              if you want an {@link Mutex.Access#uninitialized()
     *                              uninitialized} {@link Mutex}.
     */
    public Mutex(T value) throws NullPointerException {
        Objects.requireNonNull(value, "Could not create an initialized mutex from a `null` value");
        this.value = value;
    }

    /**
     * A static method alias of {@link #Mutex(Object)} that creates a {@link Mutex} initialized to
     * some value.
     *
     * @param value Initial value.
     * @param <T>   Stored value type.
     * @return A {@link Mutex} initialized to {@code value}.
     * @throws NullPointerException If {@code value} is {@code null}. Use {@link #Mutex()} instead
     *                              if you want an {@link Mutex.Access#uninitialized()
     *                              uninitialized} {@link Mutex}.
     */
    public static <T> Mutex<T> of(T value) throws NullPointerException {
        return new Mutex<>(value);
    }

    /**
     * Access the value stored in this {@link Mutex}. Only using this method you can be certain that
     * the value will be accessed in a thread-safe way (so that no other thread receives a value
     * other than the one stored in this {@link Mutex}).
     *
     * <p>
     * Note that the {@link Mutex.Access} is {@link AutoCloseable}. This means the idiomatic way to
     * access the stored value is by using a try-with-resources block, in which the current thread
     * holds the access to the mutex. See examples below.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.mutex.Mutex;
     *
     * var mutex = new Mutex<>("hello");
     * try (var access = mutex.access()) {
     *     var value = access.get();
     *     assertEquals("hello", value);
     * }
     *}
     *
     * @return An {@link Mutex.Access} to the value stored in this {@link Mutex}.
     */
    @Blocking
    public Access access() {
        return new Access();
    }

    /**
     * Perform an action on the value stored in this {@link Mutex}. This is a functional variant of
     * {@link #access()}.
     *
     * <p>
     * Note that this is a <i>terminal</i> access. This method returns {@code void} and there is no
     * way (besides {@link AtomicReference} or {@link Mutex}) to get a value out of the {@code
     * action} function.
     *
     * <p>
     * <b>Examples</b>
     *
     * <p>
     * This examples shows how to use {@link AtomicReference} to pull a value out when using this
     * method.
     * {@snippet :
     * import net.thebugmc.async.mutex.Mutex;
     * import java.util.concurrent.atomic.AtomicReference;
     *
     * var mutex = Mutex.of("hello");
     * var atomicValue = new AtomicReference<>();
     * mutex.access(access -> {
     *     var value = access.get();
     *     atomicValue.set(value);
     * });
     * assertEquals("hello", atomicValue.get());
     *}
     *
     * @param action Action to perform upon the value stored in this {@link Mutex}.
     */
    @Blocking
    public void access(Consumer<Access> action) {
        try (var access = access()) {
            action.accept(access);
        }
    }

    /**
     * Checks if this {@link Mutex} stores a {@code null}. This indicates that the {@link Mutex} has
     * not yet been filled in, someone has {@link #take() took} a value from it, or the value has
     * been set to {@code null}.
     *
     * <p>
     * Using {@link Mutex.Access#uninitialized()} is recommended instead.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.mutex.Mutex;
     *
     * var mutex = new Mutex<>();
     * assertTrue(mutex.uninitialized());
     *}
     *
     * @return {@code true} if this {@link Mutex} has no stored value.
     * @see Mutex.Access#uninitialized()
     */
    @Blocking
    public boolean uninitialized() {
        try (var access = access()) {
            return access.uninitialized();
        }
    }

    /**
     * Take a value from this {@link Mutex}. This {@link Mutex} will be {@link #uninitialized()
     * uninitialized} after this operation. See {@link Mutex.Access#uninitialized()} for more
     * details.
     *
     * @return The value stored in this {@link Mutex}.
     * @see Mutex.Access#uninitialized()
     */
    @Blocking
    public T take() {
        return set(null);
    }

    /**
     * Get the value contained in this {@link Mutex} and perform a last-chance action on it before
     * leaving the lock. For example, if the {@link Mutex} stores a {@link List}, you can perform
     * a {@link List#copyOf(Collection)} on it to get a copy while the mutex is still locked.
     *
     * @param action Value mapper.
     * @param <N>    New mapped type
     * @return The mapped value.
     */
    @Blocking
    public <N> N borrow(Function<T, N> action) {
        try (var access = access()) {
            return action.apply(access.get());
        }
    }

    /**
     * Sets the value in this {@link Mutex}, returning the previously stored value.
     *
     * @param newValue The new value.
     * @return Previous value. May be {@code null} if {@link Mutex.Access#uninitialized()}.
     */
    @Blocking
    public T set(T newValue) {
        try (var access = access()) {
            var oldValue = access.get();
            access.set(newValue);
            return oldValue;
        }
    }

    /**
     * Checks if {@code obj} is a {@link Mutex} with the same value. Note that it does not return
     * {@code true} if {@code obj} is not a {@link Mutex}, even if the inner value is equal to
     * {@code obj}.
     *
     * @param obj The object to check.
     * @return {@link Object#equals(Object)} for the stored value.
     */
    @Blocking
    public boolean equals(Object obj) {
        if (!(obj instanceof Mutex<?> mutex2))
            return false;
        try (var access1 = access()) {
            try (var access2 = mutex2.access()) {
                return access1.get().equals(access2.get());
            }
        }
    }

    /**
     * Gets a {@link Object#hashCode()} for the stored value.
     *
     * @return {@link Object#hashCode()} for the stored value.
     */
    @Blocking
    public int hashCode() {
        try (var access = access()) {
            return access.get().hashCode();
        }
    }

    /**
     * Represents access to the {@link Mutex}'s inner value.
     *
     * <p>
     * Note that the {@link Mutex.Access} is {@link AutoCloseable}. This means the idiomatic way to
     * access the stored value is by using a try-with-resources block, in which the current thread
     * holds the access to the mutex. See examples for {@link Mutex#access()}.
     *
     * @see Mutex#access()
     */
    public final class Access implements AutoCloseable {
        private boolean currentlyLocked;

        private Access() {
            lock.lock();
            currentlyLocked = true;
        }

        private void ensureLocked() throws IllegalStateException {
            if (!currentlyLocked)
                throw new IllegalStateException("This access is invalid (not holding the lock)");
        }

        /**
         * Checks if this {@link Mutex} stores a {@code null}. This indicates that the {@link Mutex}
         * has not yet been filled in, someone has {@link #take() took} a value from it, or the
         * value has been set to {@code null}.
         *
         * <p>
         * <b>Examples</b>
         * {@snippet :
         * import net.thebugmc.async.mutex.Mutex;
         *
         * var mutex = new Mutex<>();
         * try (var access = mutex.access()) {
         *     assertTrue(access.uninitialized());
         * }
         *}
         *
         * @return {@code true} if this {@link Mutex} has no stored value.
         * @throws IllegalStateException If this {@link Mutex.Access} no longer holds the lock. See
         *                               {@link #close()}.
         * @see Access#uninitialized()
         */
        public boolean uninitialized() throws IllegalStateException {
            ensureLocked();
            return value == null;
        }

        /**
         * Gets the value currently stored in this {@link Mutex}.
         *
         * <p>
         * <b>Caution</b>
         *
         * <p>
         * Be careful when pulling a value out of the {@link Mutex} - you might be mutating
         * underlying value without synchronization. The exceptions are immutable values (like
         * {@link Number}s, {@link String}s, {@link Record record}s of immutable values, etc.) and
         * other stored {@link Mutex}es and similar structures, since they have their own
         * synchronization.
         *
         * <p>
         * <b>Examples</b>
         * {@snippet :
         * import net.thebugmc.async.mutex.Mutex;
         *
         * var mutex = Mutex.of("hello");
         * try (var access = mutex.access()) {
         *     var value = access.get();
         *     assertEquals("hello", value);
         * }
         *}
         *
         * @return Stored value.
         * @throws IllegalStateException If this {@link Mutex.Access} no longer holds the lock. See
         *                               {@link #close()}.
         */
        public T get() throws IllegalStateException {
            ensureLocked();
            return value;
        }

        /**
         * Stores a new value in the associated {@link Mutex}.
         *
         * <p>
         * <b>Examples</b>
         * {@snippet :
         * import net.thebugmc.async.mutex.Mutex;
         *
         * var mutex = Mutex.of();
         * try (var access = mutex.access()) {
         *     access.set("hello");
         * }
         *
         * assertEquals(Mutex.of("hello"), mutex);
         *}
         *
         * @param value New value to store.
         * @return Previous value. May be {@code null} if {@link Mutex.Access#uninitialized()}.
         * @throws IllegalStateException If this {@link Mutex.Access} no longer holds the lock. See
         *                               {@link #close()}.
         */
        public T set(T value) throws IllegalStateException {
            ensureLocked();
            var oldValue = Mutex.this.value;
            Mutex.this.value = value;
            return oldValue;
        }

        /**
         * Closes this {@link Mutex.Access} and releases the lock on the associated {@link Mutex}.
         * After calling this method, this {@link Mutex.Access} can no longer be used.
         *
         * <p>
         * <b>Examples</b>
         * {@snippet :
         * import net.thebugmc.async.mutex.Mutex;
         *
         * var mutex = Mutex.of();
         * var access = mutex.access();
         * access.close();
         * assertThrows(IllegalStateException.class, () -> access.get());
         *}
         */
        public void close() {
            if (!currentlyLocked) return;
            try {
                lock.unlock();
            } catch (IllegalMonitorStateException ignored) {
                // safe `AutoClosable.close()` without unwanted exceptions
            } finally {
                currentlyLocked = false;
            }
        }
    }
}
